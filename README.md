# gisex

## Getting started
Questo repository contiene i file necessari all'esercitazione.

Sono presenti:

- un file raster in formato GeoTiff (.tif) contenente l'intero dem della regione FVG passo 1m
- un file raster in formato GeoTiff (.tif) contenente il geoide ITG09 per il FVG
- un file vettoriale in formato GeoPackage (.gpkg) contenente i limiti regionali (ISTAT) della regione FVG
- un file vettoriale in formato GeoPackage (.gpkg) contenente i limiti provinciali (ISTAT) della regione FVG
- un file vettoriale in formato GeoPackage (.gpkg) contenente i limiti comunali (ISTAT) della regione FVG
- un file ascii contenente la mappa valori dell'oggetto puntiforme "servizio.csv"
- un file ascii contenente la mappa valori dell'oggetto puntiforme "dimensione.csv"

Scaricarlo tramite il pulsante Download file in formato *.zip e decomprimerlo.
